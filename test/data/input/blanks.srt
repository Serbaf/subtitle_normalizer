1
00:00:00,840 --> 00:00:05,060
Let's see how to extend the functionality 
of SELECT, the command

2
00:00:05,060 --> 00:00:08,660
that we use to extract data 
in an SQL query.

3
00:00:09,790 --> 00:00:12,420
The purpose of this video is 
that you learn the LIMIT command

4
00:00:12,420 --> 00:00:15,620
and their equivalents, to extract the 
first registers of a query;

5
00:00:15,740 --> 00:00:19,050
the DISTINCT command, to remove 
duplicates and extract only

6
00:00:19,050 --> 00:00:23,160
the unique values ​​of a query; and 
the COUNT command, to summarize a query

7
00:00:23,320 --> 00:00:29,760
counting the results. Notice that 
we are going to use this simulator

8
00:00:29,970 --> 00:00:31,930
to do the tests.

9
00:00:33,870 --> 00:00:37,310
A very brief note: the 
sample database we are

10
00:00:37,310 --> 00:00:40,120
using from the grocery store 
has customers, employees,

11
00:00:40,120 --> 00:00:44,070
shippers, products, and
suppliers. Then it has

12
00:00:44,070 --> 00:00:47,120
the categories of these products, 
customer orders,

13
00:00:47,120 --> 00:00:49,720
and the details of each 
of these orders. For now, we are going to work

14
00:00:49,720 --> 00:00:53,740
with individual tables. 
SELECT asterisk FROM

15
00:00:53,740 --> 00:00:58,570
Customers WHERE Country = 
"USA" LIMIT 5. This selects

16
00:00:58,570 --> 00:01:02,270
all customer fields, 
where the country is The United States

17
00:01:02,270 --> 00:01:04,810
and only shows the top five. 
Why would we want to do that?

18
00:01:04,810 --> 00:01:08,580
Sometimes a table has a lot of data 
and we only want to see the first results

19
00:01:08,590 --> 00:01:12,110
to see the structure of the data. 
That's how it's done.

20
00:01:12,490 --> 00:01:15,940
We can see this 
directly in the simulator.

21
00:01:16,350 --> 00:01:22,060
SELECT asterisk 
FROM Customers,

22
00:01:24,380 --> 00:01:27,140
The emulator is using 
square brackets,

23
00:01:27,140 --> 00:01:31,380
which are no SQL standard. 
We can remove them. WHERE

24
00:01:32,230 --> 00:01:37,390
Country = "USA". Remember that the commands 
and the name of the fields are not case sensitive,

25
00:01:37,620 --> 00:01:38,950
but the contents of the fields are.

26
00:01:42,590 --> 00:01:45,040


27
00:01:45,450 --> 00:01:49,730
Notice that there are 13 clients from 
the United States.

28
00:01:49,740 --> 00:01:53,970
13 is not an unmanageable number, but imagine that 
there were 5 000. With LIMIT 5,

29
00:01:55,060 --> 00:01:57,340
It would give us back the 
top five, so

30
00:01:58,040 --> 00:02:00,820
we would see it in a less overwhelming way.

31
00:02:00,830 --> 00:02:03,790


32
00:02:03,790 --> 00:02:06,720
The command changes between databases. 
Each one does this in a different way.

33
00:02:06,720 --> 00:02:08,990
It is important to know

34
00:02:08,990 --> 00:02:11,840
the commands of the database we 
are working with.

35
00:02:11,840 --> 00:02:15,710
It won't be the same in Oracle, 
in SQL or in IBM.

36
00:02:15,720 --> 00:02:19,240
In w3schools.com,

37
00:02:20,760 --> 00:02:26,430
which is the manual that 
I recommend about SQL, there is an explanation

38
00:02:26,430 --> 00:02:29,960
about this. Notice that on some databases, 
in SQL server and

39
00:02:29,960 --> 00:02:33,470
in Access, it is SELECT TOP. Here 
you have the example. The

40
00:02:33,910 --> 00:02:38,100
website is in English but I 
have directly translated into Spanish.

41
00:02:38,410 --> 00:02:41,930
In MySQL the command is 
LIMIT, but Oracle 12

42
00:02:41,930 --> 00:02:48,380
the command is FETCH FIRST number
ROWS ONLY. In older versions,

43
00:02:48,390 --> 00:02:52,620
WHERE ROWNUM less than or equal to number.

44
00:02:52,630 --> 00:02:55,530
Notice that depending on the database server 
that you are using

45
00:02:55,540 --> 00:02:59,650
this command will be different. 
I wanted you to know

46
00:02:59,650 --> 00:03:02,230
this and then if you need it  
you can go look at the syntax

47
00:03:02,230 --> 00:03:05,920
of your database. 
Here you have the URL of the

48
00:03:05,920 --> 00:03:11,500
translation of the w3schools manual. 
DISTINCT is

49
00:03:12,470 --> 00:03:18,260
another command that allows us to remove 
duplicates. For example, if we want to know

50
00:03:18,770 --> 00:03:21,060
how many countries or 
from

51
00:03:23,260 --> 00:03:26,970
what country 
our clients are.

52
00:03:26,970 --> 00:03:31,890
If we were to write SELECT Country 
from Customers,

53
00:03:31,890 --> 00:03:34,770
repeated countries will come up because there will be 
several clients from the same country.

54
00:03:34,780 --> 00:03:40,150
Let's try it.

55
00:03:40,370 --> 00:03:41,990


56
00:03:43,690 --> 00:03:46,500
SELECT Country

57
00:03:48,930 --> 00:03:53,030
SELECT Country from Customers.

58
00:03:55,900 --> 00:03:59,010
Notice we have 91 results. 
If we have 91 customers,

59
00:03:59,010 --> 00:04:01,220
91 countries will appear, but they 
are repeated.

60
00:04:01,220 --> 00:04:02,810
If we use the DISTINCT command...

61
00:04:04,260 --> 00:04:07,930
Remember that it is not case sensitive 
neither in the commands nor in the name

62
00:04:07,930 --> 00:04:11,040
fields. There are 21

63
00:04:11,040 --> 00:04:15,130
different countries. That's what 
DISTINCT is for.

64
00:04:17,190 --> 00:04:22,130
Then we have COUNT. COUNT 
does what you see here in the simulator.

65
00:04:22,690 --> 00:04:25,140
The simulator shows us directly the number 
of registers it extracted.

66
00:04:26,770 --> 00:04:30,620
Many SQL clients directly show us 
this number.

67
00:04:30,620 --> 00:04:36,330
If our client doesn't, that is what COUNT  
is for.

68
00:04:36,440 --> 00:04:38,700
If we write SELECT COUNT

69
00:04:39,780 --> 00:04:44,550
between parentheses, 
It is going to give us a summary

70
00:04:46,410 --> 00:04:49,930
and it will show us the number of registers, 
21. Notice that these two numbers match.

71
00:04:49,930 --> 00:04:52,570
In this emulator the COUNT command is redundant.

72
00:04:52,900 --> 00:04:55,970
Here there is another example.

73
00:04:58,070 --> 00:05:01,530
It counts the client identifiers from 
the customer's table where the country

74
00:05:01,530 --> 00:05:06,670
is the United Kingdom. There are 7 clients from the UK.

75
00:05:06,790 --> 00:05:13,330
To summarize, we have seen 
how to get only the first

76
00:05:13,340 --> 00:05:15,720
registers from a query with LIMIT 
or with its equivalent in

77
00:05:15,720 --> 00:05:18,040
the database we are working with; 
how to eliminate

78
00:05:18,040 --> 00:05:21,210
query duplicates 
using DISTINCT, to output only

79
00:05:21,210 --> 00:05:24,790
unique results; and how to summarize 
a query counting the

80
00:05:24,790 --> 00:05:26,730
results using COUNT.

