0
00:00:00,840 --> 00:00:08,660
Let's see how to extend the functionality of SELECT, the command that we use to extract data in an SQL query.

1
00:00:09,790 --> 00:00:13,503
The purpose of this video is that you learn the LIMIT command and their equivalents,

2
00:00:13,503 --> 00:00:16,918
to extract the first registers of a query; the DISTINCT command,

3
00:00:16,918 --> 00:00:21,982
to remove duplicates and extract only the unique values ​​of a query; and the COUNT command,

4
00:00:21,982 --> 00:00:31,930
to summarize a query counting the results. Notice that we are going to use this simulator to do the tests.

5
00:00:33,870 --> 00:00:41,751
A very brief note: the sample database we are using from the grocery store has customers, employees, shippers, products,

6
00:00:41,751 --> 00:00:48,605
and suppliers. Then it has the categories of these products, customer orders, and the details of each of these orders.

7
00:00:48,605 --> 00:00:57,385
For now, we are going to work with individual tables. SELECT asterisk FROM Customers WHERE Country = "USA" LIMIT 5.

8
00:00:57,385 --> 00:01:03,496
This selects all customer fields, where the country is The United States and only shows the top five.

9
00:01:03,496 --> 00:01:04,810
Why would we want to do that?

10
00:01:04,810 --> 00:01:10,702
Sometimes a table has a lot of data and we only want to see the first results to see the structure of the data.

11
00:01:10,702 --> 00:01:22,060
That's how it's done. We can see this directly in the simulator. SELECT asterisk FROM Customers,

12
00:01:24,380 --> 00:01:33,108
The emulator is using square brackets, which are no SQL standard. We can remove them. WHERE Country = "USA".

13
00:01:33,108 --> 00:01:38,950
Remember that the commands and the name of the fields are not case sensitive, but the contents of the fields are.

14
00:01:45,450 --> 00:01:49,730
Notice that there are 13 clients from the United States.

15
00:01:49,740 --> 00:01:53,247
13 is not an unmanageable number, but imagine that there were 5 000.

16
00:01:53,247 --> 00:02:00,820
With LIMIT 5, It would give us back the top five, so we would see it in a less overwhelming way.

17
00:02:03,790 --> 00:02:06,720
The command changes between databases. Each one does this in a different way.

18
00:02:06,720 --> 00:02:11,840
It is important to know the commands of the database we are working with.

19
00:02:11,840 --> 00:02:19,240
It won't be the same in Oracle, in SQL or in IBM. In w3schools. com,

20
00:02:20,760 --> 00:02:27,077
which is the manual that I recommend about SQL, there is an explanation about this.

21
00:02:27,077 --> 00:02:33,224
Notice that on some databases, in SQL server and in Access, it is SELECT TOP. Here you have the example.

22
00:02:33,224 --> 00:02:38,100
The website is in English but I have directly translated into Spanish.

23
00:02:38,410 --> 00:02:46,434
In MySQL the command is LIMIT, but Oracle 12 the command is FETCH FIRST number ROWS ONLY.

24
00:02:46,434 --> 00:02:52,620
In older versions, WHERE ROWNUM less than or equal to number.

25
00:02:52,630 --> 00:02:57,990
Notice that depending on the database server that you are using this command will be different.

26
00:02:57,990 --> 00:03:03,593
I wanted you to know this and then if you need it you can go look at the syntax of your database.

27
00:03:03,593 --> 00:03:10,105
Here you have the URL of the translation of the w3schools manual.

28
00:03:10,105 --> 00:03:16,054
DISTINCT is another command that allows us to remove duplicates.

29
00:03:16,054 --> 00:03:21,060
For example, if we want to know how many countries or from

30
00:03:23,260 --> 00:03:31,890
what country our clients are. If we were to write SELECT Country from Customers,

31
00:03:31,890 --> 00:03:40,150
repeated countries will come up because there will be several clients from the same country. Let's try it.

32
00:03:43,690 --> 00:03:46,500
SELECT Country

33
00:03:48,930 --> 00:03:53,030
SELECT Country from Customers.

34
00:03:55,900 --> 00:04:01,220
Notice we have 91 results. If we have 91 customers, 91 countries will appear, but they are repeated.

35
00:04:01,220 --> 00:04:02,810
If we use the DISTINCT command...

36
00:04:04,260 --> 00:04:12,709
Remember that it is not case sensitive neither in the commands nor in the name fields. There are 21 different countries.

37
00:04:12,709 --> 00:04:15,130
That's what DISTINCT is for.

38
00:04:17,190 --> 00:04:22,130
Then we have COUNT. COUNT does what you see here in the simulator.

39
00:04:22,690 --> 00:04:25,140
The simulator shows us directly the number of registers it extracted.

40
00:04:26,770 --> 00:04:36,330
Many SQL clients directly show us this number. If our client doesn't, that is what COUNT is for.

41
00:04:36,440 --> 00:04:44,550
If we write SELECT COUNT between parentheses, It is going to give us a summary

42
00:04:46,410 --> 00:04:49,930
and it will show us the number of registers, 21. Notice that these two numbers match.

43
00:04:49,930 --> 00:04:55,970
In this emulator the COUNT command is redundant. Here there is another example.

44
00:04:58,070 --> 00:05:03,586
It counts the client identifiers from the customer's table where the country is the United Kingdom.

45
00:05:03,586 --> 00:05:08,425
There are 7 clients from the UK. To summarize,

46
00:05:08,425 --> 00:05:17,251
we have seen how to get only the first registers from a query with LIMIT or with its equivalent in the database we are working with;

47
00:05:17,251 --> 00:05:22,152
how to eliminate query duplicates using DISTINCT, to output only unique results;

48
00:05:22,152 --> 00:05:26,730
and how to summarize a query counting the results using COUNT.

